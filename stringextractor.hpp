#include <tuple>
#include <iostream>
#include <string>
#include <string_view>
#include <array>
#include <stdexcept>
#include <utility>
#include <charconv>


namespace AOCcom {

    namespace _helpers {
        template<typename T>
        void _adapter_helper_specialized(T& t, const std::string_view& sw) {
            const auto [ptr, errc] = std::from_chars(&sw[0], &sw[0] + sw.size(), t);

            if (ptr != (&sw[0] + sw.size()))
                std::cerr << "Error converting [" << sw << "] to type " << typeid(T).name() << ".\n";
        }

        template<typename T>
        void _adapter_helper(T& t, const std::string_view& sw) {
            t = T{sw};
        }

        template<>
        void _adapter_helper(char& t, const std::string_view& sw) {
            t = sw[0];
        }

        // TODO figure out a cleaner way of doing this
        template<>
        void _adapter_helper(int32_t& t, const std::string_view& sw) {
            // t = std::atoi(sw);
            _adapter_helper_specialized(t, sw);
        }

        template<>
        void _adapter_helper(int64_t& t, const std::string_view& sw) {
            // t = std::atol(sw);
            _adapter_helper_specialized(t, sw);
        }

        template<>
        void _adapter_helper(u_int32_t& t, const std::string_view& sw) {
            // t = std::stoul(sw);
            _adapter_helper_specialized(t, sw);
        }

        template<>
        void _adapter_helper(u_int64_t& t, const std::string_view& sw) {
            // t = std::stol(sw);
            _adapter_helper_specialized(t, sw);
        }

        template<>
        void _adapter_helper(float& t, const std::string_view& sw) {
            // t = std::stof(sw);
            _adapter_helper_specialized(t, sw);
        }

        template<>
        void _adapter_helper(double& t, const std::string_view& sw) {
            // t = std::stof(sw);
            _adapter_helper_specialized(t, sw);
        }

        template<>
        void _adapter_helper(bool& t, const std::string_view& sw) {
            t = (sw == "0" || sw == "false") ? false : true;
        }

        template<int data_count>
        std::array<std::string_view, data_count> tokenizer(const std::string& data, const std::string& data_format) {
            
            std::array<std::string_view, data_count> arr;

            auto dat_it {data.cbegin()};            const auto dat_it_end {data.cend()};
            auto fmt_it {data_format.cbegin()};     const auto fmt_it_end {data_format.cend()};

            if (dat_it == dat_it_end || fmt_it == fmt_it_end)   throw std::runtime_error("Empty data or format string!");

            // this is a test string.
            // %s   is a %s         .

            for (auto& sv : arr) {
                sv = &*data.begin();


                while (*dat_it == *fmt_it) {
                    ++dat_it;
                    ++fmt_it;

                    if (dat_it == dat_it_end || fmt_it == fmt_it_end)   throw std::runtime_error("Incorrect data or format string length!");
                }

                // at this stage two different characters are guaranteed to have been found

                if (*fmt_it != '%') throw std::runtime_error("Mismatched data and format strings!");

                // now we need to find the end of capture region

                auto next_capture {fmt_it};
                if (next_capture+1 != fmt_it_end)  // check if the character after capture identifier is end of string
                    next_capture+=2;                // stores the character after the capture identifier
                                                    // -- TODO: support for more than 1 identifier character
                                                    // will store location of the next capture in
                                                    // or the last character of the format string
                else                                
                                                    // 
                    throw std::runtime_error("Incorrect format string!");


                while (next_capture != fmt_it_end && *next_capture != '%') {
                    ++next_capture;
                }                                   // loop iterates forward to the next capture identifier
                                                    // or the end of the string

                auto null_terminator_correction {next_capture == fmt_it_end ? std::distance(dat_it, dat_it_end) : 0};

                // now find the end of the substring to capture
                auto leftover_len {static_cast<std::size_t>(std::distance(fmt_it+2, next_capture))};
                std::string_view substr {&(*fmt_it)+2, leftover_len};
                auto capture_end_pos {std::string_view{&(*dat_it)}.find(substr)}; // not finding \0

                sv = std::string_view(&*dat_it, capture_end_pos + null_terminator_correction);
                fmt_it = next_capture;
                dat_it += capture_end_pos + leftover_len;


                // std::cout << "leftover after capture id: [" << leftover_len << "]\n";
                // std::cout << "substr after capture identifier: [" << substr << "]\n";
                // std::cout << "dat_it -> [" << *dat_it << "]\nfmt_it -> [" << *fmt_it << "]\n";
                // std::cout << " ---> captured string: [" << sv << "]\n\n";
            
            }

            return arr;

        }

        template<typename tuple_t, std::size_t... index>
        auto _string_extractor_impl(const std::string& data, const std::string& data_format, std::index_sequence<index...>) {
            
            tuple_t ret_tuple;

            auto tokens {tokenizer<std::tuple_size<tuple_t>::value>(data, data_format)};

            (AOCcom::_helpers::_adapter_helper(std::get<index>(ret_tuple), tokens[index]), ...);

            return ret_tuple;

        }
    }



    template<typename... Ts>
    std::tuple<Ts...> string_extractor(const std::string& data, const std::string& data_format) {
        return AOCcom::_helpers::_string_extractor_impl<std::tuple<Ts...>>(data, data_format, std::index_sequence_for<Ts...>{});
    }


}

// struct myteststruct {
//     std::string value;

//     myteststruct() : value{"empty"} {}
//     myteststruct(const std::string& str) : value{str} {}
//     myteststruct(const std::string_view& str) : value{str} {}
// };

// std::ostream& operator<<(std::ostream& os, const myteststruct& str) {
//     return os << "myteststruct{" << str.value << "}";
// }

// auto values = AOCcom::string_extractor<int, char, float, double, myteststruct>("15 a 3.14 3.14159 doggshit value 3", "%a %a %a %a %a");

// std::cout << "[" << std::get<0>(values) << "," << std::get<1>(values) << "," << std::get<2>(values) << "," << std::get<3>(values) << "," << std::get<4>(values) << "]\n";